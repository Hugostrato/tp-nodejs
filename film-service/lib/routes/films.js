'use strict';

const Toys = require('toys');
const Joi = require('@hapi/joi');
const Films = require('../models/films');

const defaults = Toys.withRouteDefaults({
    options: {
        tags: ['api', 'films'],
    }
});

module.exports = defaults([
    {
        method: 'get',
        path: '/films',
        options: {
            response: {
                schema: Joi.array().items(
                    Joi.object({
                        id: Films.field('id'),
                        title: Films.field('title').required()
                    })
                )
            },
            auth: false
        },
        handler: (request) => {
            const {filmsService} = request.services();
            return filmsService.list();
        }
    },
    {
        method: 'get',
        path: '/film/{id}',
        options: {
            validate: {
                params: Joi.object({
                    id: Films.field('id')
                })
            },
            response: {
                emptyStatusCode: 204
            }
        },
        handler: (request) => {
            const {filmsService} = request.services();
            return filmsService.findById(request.params.id);
        }
    },
    {
        method: 'patch',
        path: '/film/{id}',
        options: {
            validate: {
                params: Joi.object({
                    id: Films.field('id')
                }),
                payload: Joi.object({
                    title: Films.field('title').required()
                })
            },
            response: {
                emptyStatusCode: 204
            }
        },
        handler: (request) => {
            const {filmsService} = request.services();
            return filmsService.update(request.params.id, request.payload);
        }
    },
    {
        method: 'post',
        path: '/film',
        options: {
            validate: {
                payload: Joi.object({
                    title: Films.field('title').required()
                })
            }
        },
        handler: (request) => {

            const {filmsService} = request.services();

            return filmsService.create(request.payload);
        }
    },
    {
        method: 'delete',
        path: '/film/{id}',
        options: {
            validate: {
                params: Joi.object({
                    id: Films.field('id')
                })
            },
            response: {
                emptyStatusCode: 204
            }
        },
        handler: async (request) => {

            const {filmsService} = request.services();

            return filmsService.delete(request.params.id);
        }
    }
]);
