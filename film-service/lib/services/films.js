'use strict';

const Schmervice = require('schmervice');
const Bcrypt = require('bcrypt');


module.exports = class FilmsService extends Schmervice.Service {
    list(ctx = {}) {
        const { Films } = this.server.models();
        return Films.query(ctx.trx);
    }

    findById(id, ctx = {}) {
        const { Films } = this.server.models();
        return Films.query(ctx.trx).findById(id);
    }


    create(film, ctx = {}) {
        const { Films } = this.server.models();

        return Films.query(ctx.trx).insert(film);
    }

    update(id, film, ctx = {}) {
        const { Films } = this.server.models();
        return Films.query(ctx.trx).patch(film).where({ id });
    }

    delete(id, ctx = {}) {
        const { Films } = this.server.models();
        return Films.query(ctx.trx).delete().where({ id });
    }
};
