'use strict';

const Schwifty = require('schwifty');
const Joi      = require('@hapi/joi');

module.exports = class Films extends Schwifty.Model {

    static field(name){
        return this.getJoiSchema().extract(name)
            .optional()
            .options({ noDefaults:true});
    }

    static get tableName() {
        return 'films';
    }
    static get joiSchema() {
        return Joi.object({
            id        : Joi.number().integer(),
            title  : Joi.string().max(255)
        });
    }
};
