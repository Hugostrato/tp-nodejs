'use strict';

module.exports = (server, options) => {

    return {
        name: 'jwt',
        scheme: 'jwt',
        options: {
            key: 'user-service-api',
            validate: async (decoded, request) => {

                return {isValid: true};

            }
        }
    };
};
