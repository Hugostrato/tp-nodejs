'use strict';

module.exports = {
    async up(knex) {

        await knex.schema.createTable('films', (table) => {

            table.increments('id');
            table.string('title').unique();
        });
    },
    async down(knex) {

        await knex.schema.dropTableIfExists('films');
    }
};
