'use strict';

const Schwifty = require('schwifty');
const Joi      = require('@hapi/joi');

module.exports = class Carts extends Schwifty.Model {

    static field(name){
        return this.getJoiSchema().extract(name)
            .optional()
            .options({ noDefaults:true});
    }

    static get tableName() {
        return 'carts';
    }
    static get joiSchema() {
        return Joi.object({
            id        : Joi.number().integer(),
            filmTitle  : Joi.string().max(255),
            username  : Joi.string().max(255),
            price  : Joi.number().integer()
        });
    }
};
