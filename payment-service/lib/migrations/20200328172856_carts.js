'use strict';

module.exports = {
    async up(knex) {

        await knex.schema.createTable('carts', (table) => {

            table.increments('id');
            table.string('filmTitle');
            table.string('username');
            table.string('price')
        });
    },
    async down(knex) {

        await knex.schema.dropTableIfExists('carts');
    }
};
