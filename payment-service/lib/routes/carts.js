'use strict';

const Toys = require('toys');
const Joi = require('@hapi/joi');
const Carts = require('../models/carts');

const defaults = Toys.withRouteDefaults({
    options: {
        tags: ['api', 'carts'],
    }
});

module.exports = defaults([
    {
        method: 'get',
        path: '/carts',
        options: {
            response: {
                schema: Joi.array().items(
                    Joi.object({
                        id: Carts.field('id'),
                        filmTitle: Carts.field('filmTitle').required(),
                        username: Carts.field('username').required()
                    })
                )
            },
            auth: false
        },
        handler: (request) => {
            const {paymentService} = request.services();
            return paymentService.list();
        }
    },
    {
        method: 'get',
        path: '/cart/{id}',
        options: {
            validate: {
                params: Joi.object({
                    id: Carts.field('id')
                })
            },
            response: {
                emptyStatusCode: 204
            },
        },
        handler: (request) => {
            const {paymentService} = request.services();
            return paymentService.findById(request.params.id);
        }
    },
    {
        method: 'patch',
        path: '/cart/{id}',
        options: {
            validate: {
                params: Joi.object({
                    id: Carts.field('id')
                }),
                payload: Joi.object({
                    filmTitle: Carts.field('filmTitle').required(),
                    username: Carts.field('username').required()
                })
            },
            response: {
                emptyStatusCode: 204
            },
        },
        handler: (request) => {
            const {paymentService} = request.services();
            return paymentService.update(request.params.id, request.payload);
        }
    },
    {
        method: 'post',
        path: '/cart',
        options: {
            validate: {
                payload: Joi.object({
                    title: Carts.field('filmTitle').required(),
                    username: Carts.field('username').required()
                })
            }
        },
        handler: (request) => {

            const {paymentService} = request.services();

            return paymentService.create(request.payload);
        }
    },
    {
        method: 'delete',
        path: '/cart/{id}',
        options: {
            validate: {
                params: Joi.object({
                    id: Carts.field('id')
                })
            },
            response: {
                emptyStatusCode: 204
            }
        },
        handler: async (request) => {

            const {paymentService} = request.services();

            return paymentService.delete(request.params.id);
        }
    }
]);
