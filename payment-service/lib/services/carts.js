'use strict';

const Schmervice = require('schmervice');
const Bcrypt = require('bcrypt');


module.exports = class PaymentService extends Schmervice.Service {
    list(ctx = {}) {
        const { Carts } = this.server.models();
        return Carts.query(ctx.trx);
    }

    findById(id, ctx = {}) {
        const { Carts } = this.server.models();
        return Carts.query(ctx.trx).findById(id);
    }


    create(cart, ctx = {}) {
        const { Carts } = this.server.models();

        return Carts.query(ctx.trx).insert(cart);
    }

    update(id, cart, ctx = {}) {
        const { Carts } = this.server.models();
        return Carts.query(ctx.trx).patch(cart).where({ id });
    }

    delete(id, ctx = {}) {
        const { Carts } = this.server.models();
        return Carts.query(ctx.trx).delete().where({ id });
    }
};
