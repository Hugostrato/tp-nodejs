'use strict';

const Toys = require('toys');
const Joi  = require('@hapi/joi');
const User = require('../models/user');

const defaults = Toys.withRouteDefaults({
    options : {
        tags     : ['api', 'user']
    }
});

module.exports = defaults([
    {
        method: 'get',
        path: '/users',
        options:{
            response : {
                schema : Joi.array().items(
                    Joi.object({
                        id : User.field('id'),
                        username  : User.field('username').required(),
                        firstName : User.field('firstName').required(),
                        lastName  : User.field('lastName').required(),
                        email     : User.field('email').required(),
                        password  : User.field('password').required(),
                        createdAt  : User.field('createdAt').required(),
                        updatedAt  : User.field('updatedAt').required()
                    })
                )
            },
            auth: false
        },
        handler: (request) => {
            const { userService } = request.services();
            return userService.list();
        }
    },
    {
        method: 'get',
        path: '/user/{id}',
        options : {
            validate : {
                params : Joi.object({
                    id : User.field('id')
                })
            },
            response : {
                emptyStatusCode : 204
            }
        },
        handler: (request) => {
            const { userService } = request.services();
            return userService.findById(request.params.id);
        }
    },
    {
        method: 'patch',
        path: '/user/{id}',
        options : {
            validate : {
                params : Joi.object({
                    id : User.field('id')
                }),
                payload : Joi.object({
                    username  : User.field('username').required(),
                    password  : User.field('password').required()
                })
            },
            response : {
                emptyStatusCode : 204
            }
        },
        handler: (request) => {
            const { userService } = request.services();
            return userService.update(request.params.id, request.payload);
        }
    },
    {
        method  : 'post',
        path    : '/user',
        options : {
            validate : {
                payload : Joi.object({
                    username  : User.field('username').required(),
                    firstName : User.field('firstName').required(),
                    lastName  : User.field('lastName').required(),
                    email     : User.field('email').required(),
                    password  : User.field('password').required()
                })
            }
        },
        handler : (request) => {
            const { userService } = request.services();

            return userService.create(request.payload);
        }
    },
    {
        method  : 'delete',
        path    : '/user/{id}',
        options : {
            validate : {
                params : Joi.object({
                    id : User.field('id')
                })
            },
            response : {
                emptyStatusCode : 204
            }
        },
        handler : async (request) => {

            const { userService } = request.services();

            return userService.delete(request.params.id);
        }
    },
    {
        method  : 'post',
        path    : '/user/login',
        options : {
            validate : {
                payload : Joi.object({
                    username  : User.field('username'),
                    password  : User.field('password')
                })
            },
            response: {
                schema: Joi.object({
                    token : Joi.string()
                })
            }
        },
        handler : async (request) => {
            const { authService } = request.services();

            return authService.login(request.payload);
        }
    }
]);
