'use strict';

module.exports = (server, options) => {

    return {
        name    : 'jwt',
        scheme  : 'jwt',
        options : {
            key      : 'user-service-api',
            validate : async (decoded, request) => {

                const { userService } = request.services();

                try {

                    await userService.findById(decoded.id);

                    return { isValid : true };
                } catch (error) {

                    return { isValid : false };
                }
            }
        }
    };
};
