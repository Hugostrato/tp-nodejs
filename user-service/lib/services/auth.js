'use strict';

const Schmervice = require('schmervice');
const Boom = require('@hapi/boom');
const Bcrypt = require('bcrypt');
const Jwt = require('jsonwebtoken');

module.exports = class AuthService extends Schmervice.Service {

    async login(user, ctx = {}) {
        const { userService } = this.server.services();

        const foundUser = await userService.findByUsername(user.username, this);
        const match = await Bcrypt.compare(user.password, foundUser.password);

        if (match) {
            return { token: Jwt.sign({ id: foundUser.id }, 'user-service-api') };
        }
        else {
            Boom.forbidden('Erreur !');
        }
    }

    encryptPassword(password) {
        return Bcrypt.hash(password, 10);
    }

};
