'use strict';

const Schmervice = require('schmervice');
const Bcrypt = require('bcrypt');

const internals = {
    removePassword : (user) => {
        delete user.password;
        return user;
    }
};

module.exports = class UserService extends Schmervice.Service {
    list(ctx = {}) {
        const { User } = this.server.models();
        return User.query(ctx.trx);
    }

    findById(id, ctx = {}) {
        const { User } = this.server.models();
        return User.query(ctx.trx).findById(id);
    }

    findByUsername(username, ctx = {}) {
        const { User } = this.server.models();
        return User.query(ctx.trx).where({ username }).first();
    }
    async create(user, ctx = {}) {
        const { User } = this.server.models();

        const { authService } = this.server.services();

        user.password = await authService.encryptPassword(user.password);

        return User.query(ctx.trx).insert(user).traverse(internals.removePassword);
    }

    update(id, user, ctx = {}) {
        const { User } = this.server.models();
        return User.query(ctx.trx).patch(user).where({ id });
    }

    delete(id, ctx = {}) {
        const { User } = this.server.models();
        return User.query(ctx.trx).delete().where({ id });
    }
};
